package org.home;

import java.io.IOException;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class ManualPlay
{
	public static void main(String[] args)
	{
		try
		{
			WordlePlayer player = new WordlePlayer();
//			System.out.println(player.getFirstWord());
			Set<String> currDict = player.doStep(new HashSet<>(player.getFullDictionary()),
					new WordlePlayer.StepParams("р", "кот", "...а.", new String[] { ".р..." }));
			currDict = player.doStep(currDict,
					new WordlePlayer.StepParams("", "едн", "...а.", new String[] { "р...." }));
			currDict = player.doStep(currDict,
					new WordlePlayer.StepParams("", "фуж", "...а.", new String[] { "..р.." }));
			currDict = player.doStep(currDict,
					new WordlePlayer.StepParams("", "сх", "...ар", new String[] { ".а..." }));
			List<String> list = Arrays.asList(currDict.toArray(new String[0]));
			list.stream().forEach(System.out::println);
			System.out.println("Best: " + player.guessGoodWord(list));
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}

	}
}
