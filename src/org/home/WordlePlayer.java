package org.home;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class WordlePlayer
{

	private final List<String> fullDict;
	private final Map<Character, Integer> stat;

	public WordlePlayer() throws IOException
	{
		try (Stream<String> stream = Files.lines(Paths.get("singular.txt")))
		{
			fullDict = stream.filter(s -> s.length() == 5).collect(Collectors.toList());
			stat = new HashMap<>();
			fullDict.forEach(s -> s.chars().forEach(c -> stat.compute((char) c, (k, v) -> v == null ? 1 : v + 1)));
		}
	}

	public int playOneGame(StringBuilder log)
	{
		//shuffle dict
		Collections.shuffle(fullDict);
		String secret = fullDict.get(0);
		log.append("Secret: ").append(secret).append("\n");
		Set<String> dict = new HashSet<>(fullDict);

		String attempt = guessGoodWord(fullDict);
		int stepCount = 0;
		do
		{
			log.append("The attempt word is: ").append(attempt).append(" dictionary size: ").append(dict.size())
					.append("\n");
			dict = doStep(dict, getStepParams(secret, attempt));
			attempt = guessGoodWord(dict);
			stepCount++;
		}
		while (dict.size() > 1);

		log.append("The answer is: ").append(attempt).append("\n");
		log.append("Steps: ").append(stepCount).append("\n");

		return stepCount;
	}

	public String getFirstWord() {
		return fullDict.stream().max((o1, o2) -> getScore(o1) - getScore(o2)).get();
	}

	public String guessGoodWord(Collection<String> dict)
	{
		return dict.parallelStream().max((o1, o2) -> getScore(o1) - getScore(o2)).get();
	}

	private Integer getScore(String s)
	{
		return s.chars().mapToObj(c -> (Character) ((char) c)).collect(Collectors.toSet()).stream().map(stat::get)
				.reduce(0, Integer::sum);
	}

	private static StepParams getStepParams(final String secret, String tryWord)
	{
		StringBuilder yesLetters = new StringBuilder();
		StringBuilder noLetters = new StringBuilder();
		StringBuilder pattern = new StringBuilder();
		List<String> antiPattern = new ArrayList<>();

		for (int i = 0; i < tryWord.length(); i++)
		{
			if (tryWord.charAt(i) == secret.charAt(i))
			{
				pattern.append(tryWord.charAt(i));
			}
			else
			{
				pattern.append(".");
				if (secret.contains(Objects.toString(tryWord.charAt(i))))
				{
					yesLetters.append(tryWord.charAt(i));
					//add to antiPattern
					antiPattern.add(".".repeat(i) + tryWord.charAt(i) + ".".repeat(tryWord.length() - i - 1));
				}
				else
				{
					noLetters.append(tryWord.charAt(i));
				}
			}
		}

		return new StepParams(yesLetters.toString(), noLetters.toString(), pattern.toString(),
				antiPattern.toArray(new String[0]));
	}

	public Set<String> doStep(Set<String> dict, StepParams stepParams)
	{
		return dict.parallelStream().filter(s -> s.matches(stepParams.getPattern()))
				.filter(s -> Arrays.stream(stepParams.getAnti()).noneMatch(s::matches))
				.filter(s -> stepParams.getYesLetters().chars().allMatch(s1 -> s.contains(Objects.toString((char) s1))))
				.filter(s -> stepParams.getNoLetters().chars().noneMatch(s1 -> s.contains(Objects.toString((char) s1))))
				.collect(Collectors.toSet());
	}

	public void shuffleDict()
	{
		Collections.shuffle(fullDict);
	}

	public List<String> getFullDictionary()
	{
		return fullDict;
	}

	public static class StepParams
	{
		private final String yesLetters;
		private final String noLetters;
		private final String pattern;
		private final String[] anti;

		public StepParams(String yesLetters, String noLetters, String pattern, String[] anti)
		{
			this.yesLetters = yesLetters;
			this.noLetters = noLetters;
			this.pattern = pattern;
			this.anti = anti;
		}

		public String getYesLetters()
		{
			return yesLetters;
		}

		public String getNoLetters()
		{
			return noLetters;
		}

		public String getPattern()
		{
			return pattern;
		}

		public String[] getAnti()
		{
			return anti;
		}
	}

}
