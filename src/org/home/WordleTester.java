package org.home;

import java.io.IOException;

public class WordleTester
{

	public static final int GAME_COUNT = 1000;

	public static void main(String[] args)
	{
		try
		{
			WordlePlayer player = new WordlePlayer();

			StringBuilder log = new StringBuilder();
			double avgSteps = 0;
			int maxSteps = 0;
			int minSteps = Integer.MAX_VALUE;
			long time = System.currentTimeMillis();
			for (int i = 0; i < GAME_COUNT; i++)
			{
				log.append("Game #").append(i).append(": ").append("\n");
				int steps = player.playOneGame(log);
				maxSteps = Math.max(maxSteps, steps);
				minSteps = Math.min(minSteps, steps);
				avgSteps += steps;
			}
			avgSteps /= GAME_COUNT;

			log.append("Max steps: ").append(maxSteps).append("\n");
			log.append("Min steps: ").append(minSteps).append("\n");
			log.append("Average steps: ").append(avgSteps).append("\n");
			log.append("Time: ").append(System.currentTimeMillis() - time).append(" ms").append("\n");

			System.out.println(log);
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}

	}

}
